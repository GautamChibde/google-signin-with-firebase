package com.example.gautam.googlesigninwithfirebase

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn


class MainActivity : AppCompatActivity() {

    private lateinit var authUtils: GoogleAuthUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        authUtils = GoogleAuthUtils(this)
        startActivityForResult(
                GoogleSignIn.getClient(this, authUtils.googleSignInOptions).signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            authUtils.handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
    }

    companion object {
        const val RC_SIGN_IN = 9001
    }
}
